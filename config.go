package curl

import "sync"

var config = struct {
	sync.RWMutex

	Debug bool
}{
	Debug: false,
}

func IsDebug() bool {
	config.RLock()
	defer config.RUnlock()

	return config.Debug
}

func Debug(debug bool) {
	config.Lock()
	defer config.Unlock()

	config.Debug = debug
}
