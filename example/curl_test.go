package goz

import (
	"fmt"
	"gitee.com/teval/curl"
	"gitee.com/teval/goz"
)

func ExampleGet() {
	b, e := curl.Get("http://127.0.0.1:8091/get", goz.Options{
		Query: map[string]interface{}{
			"key": "test_key",
		},
	})
	if e != nil {
		fmt.Println(e.Error())
		return
	}
	fmt.Printf(`%s`, b.GetContents())
	// Output: key=test_key
}

func ExamplePost_form() {
	b, e := curl.Post("http://127.0.0.1:8091/post", goz.Options{
		FormParams: map[string]interface{}{
			"key1": "value1",
			"key2": "value2",
		},
	})
	if e != nil {
		fmt.Println(e)
		return
	}
	fmt.Printf(`%s`, b.GetContents())
	// Output: {"key1":"value1","key2":"value2"}
}

func ExamplePost_json() {
	b, e := curl.Post("http://127.0.0.1:8091/post", goz.Options{
		JSON: map[string]interface{}{
			"key1": "value1",
			"key2": "value2",
		},
	})
	if e != nil {
		fmt.Println(e)
		return
	}
	fmt.Printf(`%s`, b.GetContents())
	// Output: {"key1":"value1","key2":"value2"}
}
