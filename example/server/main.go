package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/get", Get)
	r.POST("/post", Post)

	fmt.Println(r.Run(":8091"))
}

func Get(c *gin.Context) {
	c.String(200, `key=%s`, c.Query("key"))
}

func Post(c *gin.Context) {
	var params struct {
		Key1 string `json:"key1" form:"key1"`
		Key2 string `json:"key2" form:"key2"`
	}

	if c.ShouldBind(&params) == nil {
		if str, err := json.Marshal(params); err == nil {
			c.String(200, string(str))
		}
	}
}
